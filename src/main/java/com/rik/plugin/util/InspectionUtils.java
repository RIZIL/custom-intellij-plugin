package com.rik.plugin.util;

import java.util.Arrays;

import com.intellij.psi.xml.XmlTag;

public class InspectionUtils {

    private InspectionUtils() {
    }

    public static boolean isColumnNotNullWithoutDefaultValue(XmlTag column) {
        return isColumnNotNull(column) && !hasDefaultValue(column);
    }

    public static boolean isColumnNotNull(XmlTag column) {
        return Arrays.stream(column.getSubTags()).anyMatch(subTag -> "constraints".equals(subTag.getName()) && isConstraintNotNull(subTag));
    }

    public static boolean isConstraintNotNull(XmlTag constraint) {
        String nullable = constraint.getAttributeValue("nullable");
        return nullable != null && nullable.equals("false");
    }

    public static boolean hasDefaultValue(XmlTag column) {
        return Arrays.stream(column.getAttributes()).anyMatch(atr -> atr.getName().contains("default"));
    }

    public static boolean isMissingColumnType(XmlTag column) {
        String name = column.getAttributeValue("name");
        return name != null && !name.isEmpty() && column.getAttributeValue("type") == null;
    }
}
