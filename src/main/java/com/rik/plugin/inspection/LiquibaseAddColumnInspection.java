package com.rik.plugin.inspection;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.*;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.intellij.codeInspection.LocalInspectionTool;
import com.intellij.codeInspection.ProblemsHolder;
import com.intellij.codeInspection.ui.MultipleCheckboxOptionsPanel;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.XmlElementVisitor;
import com.intellij.psi.xml.XmlTag;
import com.intellij.psi.xml.XmlTagChild;
import com.rik.plugin.inspection.quickfix.AddColumnTypeFix;
import com.rik.plugin.inspection.quickfix.AddDefaultValueFix;
import com.rik.plugin.inspection.quickfix.AddDeleteTableCascadeFix;
import com.rik.plugin.util.InspectionUtils;

//todo: action -> reorder pom dependencies by the alphabet with the regard to the empty lines/separators
public class LiquibaseAddColumnInspection extends LocalInspectionTool {

    private static final String ADDING_NOT_NULL_COLUMN_NAME = "ADDING_NOT_NULL_COLUMN";

    private static final String MISSING_TYPE_NAME = "MISSING_TYPE";

    public boolean ADDING_NOT_NULL_COLUMN = true;

    public boolean MISSING_TYPE = true;

    @Override
    public @Nullable JComponent createOptionsPanel() {
        MultipleCheckboxOptionsPanel optionsPanel = new MultipleCheckboxOptionsPanel(this);
        optionsPanel.addCheckbox("Adding NOT NULL column", ADDING_NOT_NULL_COLUMN_NAME);
        optionsPanel.addCheckbox("Missing type when adding new column", MISSING_TYPE_NAME);
        return optionsPanel;
    }

    @Override
    public @NotNull PsiElementVisitor buildVisitor(@NotNull ProblemsHolder holder, boolean isOnTheFly) {
        return new XmlElementVisitor() {

            @Override
            public void visitXmlTag(XmlTag tag) {
                if (MISSING_TYPE && ADDING_NOT_NULL_COLUMN && "changeSet".equals(tag.getName())) {
                    List<XmlTag> addColumnTags = Arrays.stream(tag.getSubTags()).filter(subTag -> "addColumn".equals(subTag.getName())).collect(Collectors.toList());
                    for (XmlTag addColumn : addColumnTags) {
                        List<XmlTag> columns = Arrays.stream(addColumn.getSubTags()).filter(subTag -> "column".equals(subTag.getName())).collect(Collectors.toList());
                        checkMissingType(columns);
                        checkNotNullColumns(addColumn, columns);
                    }
                }
                super.visitXmlTag(tag);
            }

            private void checkMissingType(List<XmlTag> columns) {
                if (MISSING_TYPE) {
                    columns.stream().filter(InspectionUtils::isMissingColumnType)
                            .forEach(column -> holder.registerProblem(column.getOriginalElement(), "Missing type.",
                                    new AddColumnTypeFix("BIGINT"), new AddColumnTypeFix("VARCHAR(255)"), new AddColumnTypeFix("NUMERIC"),
                                    new AddColumnTypeFix("TIMESTAMP"), new AddColumnTypeFix("INT(10)")));
                }
            }

            private void checkNotNullColumns(XmlTag addColumn, List<XmlTag> columns) {
                if (ADDING_NOT_NULL_COLUMN) {
                    String tableName = addColumn.getAttributeValue("tableName");
                    boolean tableIsDeleted = tableIsDeleted(addColumn, tableName);
                    if (tableIsDeleted) {
                        return;
                    }
                    for (XmlTag column : columns) {
                        if (InspectionUtils.isColumnNotNullWithoutDefaultValue(column)) {
                            holder.registerProblem(column.getOriginalElement(), String.format("Adding NOT NULL column. Define defaultValue or delete table '%s'.", tableName),
                                    new AddDeleteTableCascadeFix(), new AddDefaultValueFix());
                        }
                    }
                }
            }

            private boolean tableIsDeleted(XmlTag addColumn, String tableName) {
                if (tableName == null) {
                    return true;
                }
                XmlTagChild previousSibling = addColumn.getPrevSiblingInTag();
                while (previousSibling != null) {
                    try {
                        XmlTag tag = ((XmlTag) previousSibling.getNode());
                        if ("delete".equals(tag.getName()) && tableName.equals(tag.getAttributeValue("tableName"))) {
                            return true;
                        }
                    } catch (ClassCastException e) {
                        // ignore white space
                    }
                    previousSibling = previousSibling.getPrevSiblingInTag();
                }
                return false;
            }
        };
    }
}
