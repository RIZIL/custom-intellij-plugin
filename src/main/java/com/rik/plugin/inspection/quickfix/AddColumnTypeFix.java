package com.rik.plugin.inspection.quickfix;

import org.jetbrains.annotations.NotNull;

import com.intellij.codeInspection.LocalQuickFix;
import com.intellij.codeInspection.ProblemDescriptor;
import com.intellij.codeInspection.util.IntentionFamilyName;
import com.intellij.openapi.project.Project;
import com.intellij.psi.XmlElementFactory;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlTag;

public class AddColumnTypeFix implements LocalQuickFix {
    private final String type;

    public AddColumnTypeFix(String type) {
        this.type = type;
    }

    @Override
    public @IntentionFamilyName @NotNull String getFamilyName() {
        return "Add type " + type;
    }

    @Override
    public void applyFix(@NotNull Project project, @NotNull ProblemDescriptor descriptor) {
        XmlTag column = ((XmlTag) descriptor.getPsiElement().getNode());
        XmlAttribute typeAttribute = XmlElementFactory.getInstance(project).createAttribute("type", this.type, column);
        column.add(typeAttribute);
    }
}
