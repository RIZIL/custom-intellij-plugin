package com.rik.plugin.inspection.quickfix;

import org.jetbrains.annotations.NotNull;

import com.intellij.codeInspection.LocalQuickFix;
import com.intellij.codeInspection.ProblemDescriptor;
import com.intellij.codeInspection.util.IntentionFamilyName;
import com.intellij.openapi.project.Project;

//todo: action -> add delete table ...with cascading if it's not defined
public class AddDeleteTableCascadeFix implements LocalQuickFix {
    @Override
    public @IntentionFamilyName @NotNull String getFamilyName() {
        return "Add delete table";
    }

    @Override
    public void applyFix(@NotNull Project project, @NotNull ProblemDescriptor descriptor) {
//            descriptor.getPsiElement() == addColumn
    }
}
