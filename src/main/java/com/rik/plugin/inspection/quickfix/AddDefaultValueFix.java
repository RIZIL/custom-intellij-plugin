package com.rik.plugin.inspection.quickfix;

import org.jetbrains.annotations.NotNull;

import com.intellij.codeInspection.LocalQuickFix;
import com.intellij.codeInspection.ProblemDescriptor;
import com.intellij.codeInspection.util.IntentionFamilyName;
import com.intellij.openapi.editor.CaretModel;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.LogicalPosition;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileEditor.TextEditor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiDocumentManager;
import com.intellij.psi.PsiElement;
import com.intellij.psi.XmlElementFactory;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlTag;

import static java.util.Objects.requireNonNull;

public class AddDefaultValueFix implements LocalQuickFix {
    @Override
    public @IntentionFamilyName @NotNull String getFamilyName() {
        return "Add default value";
    }

    @Override
    public void applyFix(@NotNull Project project, @NotNull ProblemDescriptor descriptor) {
        XmlTag column = ((XmlTag) descriptor.getPsiElement().getNode());

        XmlAttribute defaultValueAttribute = XmlElementFactory.getInstance(project).createAttribute("defaultValue", "", column);
        column.add(defaultValueAttribute);

        moveCaret(project, column);
    }

    private void moveCaret(@NotNull Project project, XmlTag column) {
        TextEditor editor = requireNonNull(((TextEditor) FileEditorManager.getInstance(project).getSelectedEditor()));
        Document document = requireNonNull(PsiDocumentManager.getInstance(project).getDocument(column.getContainingFile()));
        CaretModel caretModel = editor.getEditor().getCaretModel();
        caretModel.removeSecondaryCarets();
        caretModel.moveToLogicalPosition(createLogicalPosition(document, column));
    }

    private LogicalPosition createLogicalPosition(Document document, PsiElement element) {
        int lineNumber = document.getLineNumber(element.getTextOffset());
        int columnNumber = document.getLineEndOffset(lineNumber) - element.getTextOffset() + 10;
        return new LogicalPosition(lineNumber, columnNumber);
    }
}
